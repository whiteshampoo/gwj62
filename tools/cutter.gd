@tool
extends Node

@export var cut: bool = false:
	set = update
@export var source: Image = null
@export_dir var target_folder: String = "res://"
@export var tile_size: Vector2i = Vector2i(16, 16)

func update(__: bool = false) -> void:
	if not source.get_width() % tile_size.x == 0:
		push_error("Wrong width")
		return
	if not source.get_height() % tile_size.y == 0:
		push_error("Wrong height")
		return
	for x in source.get_width() / tile_size.x:
		for y in source.get_height() / tile_size.y:
			var image: Image = source.get_region(Rect2i(
				x * tile_size.x,
				y * tile_size.y,
				tile_size.x,
				tile_size.y
			))
			image.save_webp(target_folder.path_join("%02d_%02d.webp" % [x, y]))
