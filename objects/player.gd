extends CharacterBody3D


@export var speed: float = 5.0
@export var jump_velocity: float = 4.5

@onready var camera: Camera3D = $Camera
@onready var ray: RayCast3D = $Camera/Ray

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity: float = ProjectSettings.get_setting("physics/3d/default_gravity")


func _ready() -> void:
	DisplayServer.mouse_set_mode(DisplayServer.MOUSE_MODE_CAPTURED)
	InputHandler.connect_pressed("cancel", get_tree().quit)


func _input(event: InputEvent) -> void:
	if event is InputEventMouseMotion:
		mouse_motion(event)
	elif event is InputEventMouseButton:
		mouse_button(event)

func _physics_process(delta: float) -> void:


	velocity.y -= gravity * delta

	if Input.is_action_just_pressed("ui_accept") and is_on_floor():
		velocity.y = jump_velocity

	var input_dir : Vector2 = InputHandler.get_vector()
	var direction : Vector3 = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	velocity.x = direction.x * speed
	velocity.z = direction.z * speed

	move_and_slide()

func mouse_motion(event: InputEventMouseMotion) -> void:
	rotate_y(-event.relative.x * TAU / 1000.0)
	camera.rotate_x(-event.relative.y * TAU / 1000.0)
	camera.rotation.x = clampf(camera.rotation.x, -PI / 2.0, PI / 2.0)


func mouse_button(event: InputEventMouseButton) -> void:
	if not event.button_index == MOUSE_BUTTON_LEFT:
		return
	if not event.pressed:
		return
	var collider: Object = ray.get_collider()
	if collider is GridMap:
		var gridmap: GridMap = collider as GridMap
		var coordinate: Vector3 = ray.get_collision_point()
		var normal: Vector3 = ray.get_collision_normal() / 2.0
		if is_equal_approx(coordinate.x, roundf(coordinate.x)):
			coordinate.x -= normal.x
		if is_equal_approx(coordinate.y, roundf(coordinate.y)):
			coordinate.y -= normal.y
		if is_equal_approx(coordinate.z, roundf(coordinate.z)):
			coordinate.z -= normal.z
		prints(collider.to_local(coordinate), collider.local_to_map(collider.to_local(coordinate)))
		collider.set_cell_item(collider.local_to_map(collider.to_local(coordinate)), -1)
	elif collider is Door:
		collider.use()
	
		
