extends Area3D

@export var front: Texture2D
@export var back: Texture2D
@export var left: Texture2D
@export var right: Texture2D

@onready var camera: Camera3D = get_viewport().get_camera_3d()
@onready var mesh: MeshInstance3D = $Mesh


func _physics_process(delta: float) -> void:
	var my_pos2d: Vector2 = Vector2(global_position.x, global_position.z)
	var cam_pos2d: Vector2 = Vector2(camera.global_position.x, camera.global_position.z)
	var sector: int = int(round(remap(my_pos2d.angle_to_point(cam_pos2d),-PI, PI, 0, 7)))
	print(sector)
	match sector:
		5, 6:
			mesh.mesh.material.albedo_texture = front
		7, 0: #right
			mesh.mesh.material.albedo_texture =right
		1, 2: # left
			mesh.mesh.material.albedo_texture = back
		3, 4:
			mesh.mesh.material.albedo_texture = left
