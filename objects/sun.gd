@tool
extends Node3D

@export_range(0.0, 1.0) var time: float = 0.0:
	set(new_time):
		time = wrapf(new_time, 0.0, 24.0)
		if not red or not green or not blue:
			return
		color = Color(red.sample(time), green.sample(time), blue.sample(time))
		if not is_instance_valid(environment):
			set_deferred("time", time)
			return
		environment.environment.sky.sky_material.sky_top_color = sky_top * color
		environment.environment.sky.sky_material.sky_horizon_color = sky_horizon * color
		environment.environment.ambient_light_sky_contribution = color.v / 5.0
		light.light_color = color
		rotation.x = time * TAU

@export var red: Curve = Curve.new()
@export var green: Curve = Curve.new()
@export var blue: Curve = Curve.new()
@export var color: Color = Color()

@export var sky_top: Color = Color("62748c")
@export var sky_horizon: Color = Color("a5a7ab")

@onready var environment: WorldEnvironment = $Environment
@onready var light: DirectionalLight3D = $Light


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	pass
