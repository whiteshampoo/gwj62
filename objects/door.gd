class_name Door
extends StaticBody3D

@onready var animation: AnimationPlayer = $"../../Animation"

var is_open: bool = false

func use() -> void:
	if animation.is_playing():
		return
	if not is_open:
		animation.play("open_a")
	else:
		animation.play_backwards("open_a")
	is_open = not is_open
